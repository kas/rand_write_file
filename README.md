# rand_write_file

Write random bytes to any file.

Intended as a quick and dirty way to write cryptographically secure data to
the `~/.rnd` seed — used by OpenSSL's `RAND_(load|write)_file` (hence the
name) but may be used to write any number of bytes to any file.

WIP — do not use for anything serious.

### Requirements

Python 3.8+ (tested under Python 3.10 only).
