#!/usr/bin/env python
"""
Write a number of random bytes to a file.

Usage: rand_write_file [OPTIONS] [RANDFILE]
"""
#  Author: Klaus Alexander Seistrup
# License: GNU Affero General Public License v3+
#
# [Metadata just above main() method]

import argparse
import hashlib
import locale
import os
import pathlib
import secrets
import shutil
import sys
import tempfile

try:
    from typing import Union
    if sys.version_info < (3, 8):  # Because vermin says so
        raise ValueError('Python 3.8+ required')
    _ = locale.setlocale(locale.LC_ALL, '')
except (ValueError, ImportError, ModuleNotFoundError, locale.Error) as oops:
    print(oops, file=sys.stderr)
    sys.exit(1)

# Environment variables
ENV = os.environ
# Should we print debug statements?
DEBUG = ENV.get('DEBUG', 'False')[0].casefold() in '1jty'  # 1/Ja/True/Yes

# For use with type annotations
ErrorLike = Union[Exception, str]

RANDFILE = '~/.rnd'
DEF_SIZE = 1024
DEF_ITER = 1024


def die(reason: ErrorLike = '') -> None:
    """Exit gracefully, possibly with an explanation."""
    if reason:
        print(reason, file=sys.stderr)

    sys.exit(1 if reason else 0)


def read_rnd_file(fpath: pathlib.Path, size: int = DEF_SIZE) -> bytes:
    """Read and return the current contents of FNAME or ~/.rnd."""
    blob = b''

    if fpath.exists() and fpath.is_file():
        with fpath.open('rb') as fptr:
            blob = fptr.read()

    if len(blob) >= size:
        return blob

    return blob + secrets.token_bytes(max(size, DEF_SIZE))


def write_rnd_file(fpath: pathlib.Path, blob: bytes) -> str:
    """Write the binary BLOB to ~/.rnd."""
    if fpath.exists() and not fpath.is_file():
        die(f'not a file: {fpath}')

    # As an extra precation, set umask to 0077
    _ = os.umask(0o077)
    (fdesc, tempname) = tempfile.mkstemp()
    with os.fdopen(fdesc, 'wb') as fptr:
        fptr.write(blob)
    _ = shutil.move(tempname, fpath)
    return tempname


def get_rand_blob(seed: bytes = b'', size: int = DEF_SIZE) -> bytes:
    """Construct and return a random BLOB of SIZE, possibly using SEED."""
    max_size = max(size, DEF_SIZE)
    if len(seed) < max_size:
        seed += secrets.token_bytes(max_size)

    data = secrets.token_bytes(max_size)
    blob = hashlib.pbkdf2_hmac(
        'sha512', data, seed, iterations=DEF_ITER, dklen=size
    )
    return blob


############
# Metadata
__whoami__ = 'rand_write_file'
__revision__ = '2023-01-30'
__version__ = '0.0.3'
__author__ = 'Klaus Alexander Seistrup <klaus@seistrup.dk>'
__copyright__ = f"""{__whoami__}/{__version__} ({__revision__})

Copyright © 2023 Klaus Alexander Seistrup <klaus@seistrup.dk>

Licensed under the GNU Affero General public License v3+.
"""
__epilog__ = f"""
This script will write NUM_BYTES cryptographically secure random
bytes to RANDFILE, possibly using the previous file contents as
HMAC salt.

See also RAND_load_file(3), RAND_write_file(3).
"""
############


def main(progname: str) -> int:
    """Enter program here.

    Is that ”imperative mood” enough for you, flake8?
    """
    parser = argparse.ArgumentParser(
        prog=progname,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=__epilog__,
    )
    # Generic arguments
    parser.add_argument(
        '-V',
        '--version',
        action='version',
        version=f'%(prog)s/{__version__} ({__revision__})',
        help='show version information and exit',
    )
    parser.add_argument(
        '-C',
        '--copyright',
        action='version',
        version=__copyright__,
        help='show copying policy and exit',
    )
    parser.add_argument(
        '-n',
        '--num-bytes',
        type=int,
        default=DEF_SIZE,
        help=f'write NUM_BYTES to RANDFILE (default: {DEF_SIZE})',
    )
    parser.add_argument(
        'RANDFILE',
        nargs='?',
        default=RANDFILE,
        help=f'use non-default RANDFILE (default: {RANDFILE})',
    )
    args = parser.parse_args()
    fpath = pathlib.Path(args.RANDFILE or RANDFILE)
    randfile = fpath.expanduser().resolve()
    size = args.num_bytes

    if size < 1:
        die(f'NUM_BYTES must be a positive integer (was {size}).')

    try:
        seed = read_rnd_file(randfile)
        blob = get_rand_blob(seed, size)
        name = write_rnd_file(randfile, blob)
        temp = pathlib.Path(name)

        if temp.exists() and temp.is_file():
            temp.unlink(missing_ok=True)

        if DEBUG and sys.stdout.isatty():
            print(f'Wrote {len(blob)} bytes to {randfile}.')
    except (KeyboardInterrupt, BrokenPipeError):
        die()
    except (FileNotFoundError, PermissionError) as error:
        die(error)
    except IsADirectoryError as error:
        die(error)
    except (IOError, OSError, MemoryError) as error:
        die(error)

    return 0


if __name__ == '__main__':
    sys.exit(main(pathlib.Path(__file__).name))

# eof
